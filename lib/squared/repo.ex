defmodule Squared.Repo do
  use Ecto.Repo,
    otp_app: :squared,
    adapter: Ecto.Adapters.Postgres
end
