defmodule SquaredWeb.PageController do
  use SquaredWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
